package com.example.administrator.bluetoothsample;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Ohchang, Kwon
 * 설명: 블루투스 시리얼 통신 관련 모듈
 */
public class BluetoothSerial {
    public static final int SUCCESS_TO_CONNECT = 0;
    public static final int FAIL_TO_CONNECT = 1;
    public static final int SCAN_NEW_DEVICE = 2;

    Context mContext;

    BluetoothAdapter mBluetoothAdapter;
    ConnectThread connection;

    Set<BluetoothDevice> pairedDevices;
    Set<BluetoothDevice> scannedDevices;

    Handler socketConnectHandler;

    public BluetoothSerial(Context context, BluetoothAdapter adapter) {
        this.mContext = context;
        this.mBluetoothAdapter = adapter;
    }

    //  설명: 초기화용 메소드
    public void init() {
        pairedDevices = mBluetoothAdapter.getBondedDevices();
        scannedDevices = new LinkedHashSet<>();
    }

    //  설명: 주변에 Bluetooth 기기를 Scan 하는 메소드
    //        기기를 찾을 때마다 callback 실행
    //  인자:
    //        callback - Scan이 이루어질 때마다 실행하기를 원하는 Runnable 객체
    public void startDiscovery(Runnable callback) {
        // BluetoothDevice.ACTION_FOUND Intent에 대응하는 새로운 broadcastReceiver 등록
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mContext.registerReceiver(new BluetoothDeviceFindReceiver(callback), filter);

        mBluetoothAdapter.startDiscovery();
    }
    public void stopDiscovery() {
        mBluetoothAdapter.cancelDiscovery();
    }

    public Set<BluetoothDevice> getPairedDevices() {
        return pairedDevices;
    }

    public Set<BluetoothDevice> getScannedDevices() {
        return scannedDevices;
    }

    public void setSocketConnectHandler(Handler socketConnectHandler) {
        this.socketConnectHandler = socketConnectHandler;
    }

    //  설명:   전달받은 기기를 바탕으로 새로운 connection을 생성,
    //  인자:
    //          device - 새로운 연결을 원하는 BluetoothDevice 객체
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void connectDevice(BluetoothDevice device) {
        if (connection != null) {
            try {
                connection.cancel();
                connection = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // 만약에 현재 페어링되어있지 않은 기기라면 새로운 페어링 생성,
        // 성공적으로 페어링이 완료되면 그 기기로 다시 연결 시도
        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            IntentFilter pairedFilter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            mContext.registerReceiver(new BluetoothDeviceBondReceiver(), pairedFilter);

            device.createBond();
            return;
        }

        mBluetoothAdapter.cancelDiscovery();

        connection = new ConnectThread(device);
        connection.start();
    }

    public void write(byte[] bytes) throws SocketUnconnectedException, IOException {
        if (connection == null)
            throw new SocketUnconnectedException();

        connection.write(bytes);
    }
    public void closeConnection() throws IOException {
        if (connection == null)
            return;

        connection.cancel();
    }

    //// inner classes

    // BluetoothDeviceFindReceiver start
    private class BluetoothDeviceFindReceiver extends BroadcastReceiver {
        private Runnable callback;

        public BluetoothDeviceFindReceiver(Runnable callback) {
            this.callback = callback;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                scannedDevices.add(device);

                new Thread(callback).start();
            }
        }
    }
    // BluetoothDeviceFindReceiver end

    // BluetoothDeviceBondReceiver start
    // 설명: BluetoothDevice.ACTION_BOND_STATE_CHANGED intent에 대응하기 위한 BroadcastReceiver
    private class BluetoothDeviceBondReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int bondState = intent
                    .getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE);

            // 성공적으로 페어링이 되었다면 해당 기기로 다시 connect 시도
            if (BluetoothDevice.BOND_BONDED == bondState) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                connectDevice(device);
            }
        }
    }
    // bluetoothDeviceBondReceiver end

    // ConnectThread start
    // 설명: 블루투스 연결 시도를 하기 위한 쓰레드
    private class ConnectThread extends Thread {
        private BluetoothSocket socket;
        private BluetoothDevice device;
        private OutputStream socketOutput;

        public ConnectThread(BluetoothDevice device) {
            this.device = device;

            Method m;
            try {
                m = device.getClass().getMethod("createInsecureRfcommSocket", int.class);
                // 패러미터 : BluetoothDevice, port(int)
                socket = (BluetoothSocket)m.invoke(device, 1);
            }
            catch(NoSuchMethodException e){
                e.printStackTrace();
            }
            catch(IllegalAccessException e){
                e.printStackTrace();
            }
            catch(InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        public void write(byte[] bytes) throws IOException {
            socketOutput.write(bytes);
        }

        public void cancel() throws IOException {
            socket.close();
        }

        @Override
        public void run() {
            try {
                socket.connect();
                socketOutput = socket.getOutputStream();
                Message msg = socketConnectHandler.obtainMessage(
                        BluetoothSerial.SUCCESS_TO_CONNECT, device
                );
                socketConnectHandler.sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
                Message msg = socketConnectHandler.obtainMessage(
                        BluetoothSerial.FAIL_TO_CONNECT, e
                );
                socketConnectHandler.sendMessage(msg);
            }
        }
    }
    // ConnectThread end

    public class SocketUnconnectedException extends Exception {
        @Override
        public String getMessage() {
            return "it needs connectDevice()";
        }
    }
}