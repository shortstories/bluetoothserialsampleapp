package com.example.administrator.bluetoothsample;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_ENABLE_BT = 1;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSerial serial;

    TextView labelCurrentConnected;
    ListView listPairedDevices;
    ListView listScannedDevices;

    // 블루투스 시리얼 모듈에 전달하기 위해 구현한 핸들러
    @SuppressLint("HandlerLeak")
    Handler bluetoothSerialHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                // 블루투스 연결이 성공적으로 실행되었을 때
                case BluetoothSerial.SUCCESS_TO_CONNECT:
                    Toast.makeText(MainActivity.this, "연결 되었습니다.", Toast.LENGTH_LONG).show();
                    String temp = "Current connected : " + ((BluetoothDevice)msg.obj).getName();
                    labelCurrentConnected.setText(temp);

                    try {
                        // 여기에 라즈베리파이로 전송할 7바이트 16진수 데이터를
                        // String 형식으로 입력해주시면 됩니다.
                        serial.write("FF:FF:FF:FF:FF:FF:FF".getBytes());
                    } catch (BluetoothSerial.SocketUnconnectedException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                // 블루투스 연결에 실패하였을 때
                case BluetoothSerial.FAIL_TO_CONNECT:
                    Toast.makeText(MainActivity.this, "연결에 실패하였습니다.",
                            Toast.LENGTH_LONG).show();
                    break;
                // 스캔 과정에서 새로운 기기를 발견하였을 때
                case BluetoothSerial.SCAN_NEW_DEVICE:
                    ((BaseAdapter) listScannedDevices.getAdapter()).notifyDataSetChanged();
            }
        }
    };

    // 스캔에 성공하였을 때 실행시킬 Callback
    Runnable discoveryCallback = new Runnable() {
        @Override
        public void run() {
            Message msg = bluetoothSerialHandler.obtainMessage();
            msg.what = BluetoothSerial.SCAN_NEW_DEVICE;
            msg.sendToTarget();
        }
    };

    public Dialog getBluetoothDeviceDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_bluetooth, null);

        listPairedDevices = (ListView) dialogView.findViewById(R.id.list_paired_devices);
        listScannedDevices = (ListView) dialogView.findViewById(R.id.list_scanned_devices);

        serial.init();
        serial.startDiscovery(discoveryCallback);

        OnDeviceClickListener listener = new OnDeviceClickListener(getApplicationContext());
        listPairedDevices.setAdapter(new DevicesAdapter(this, serial.getPairedDevices()));
        listPairedDevices.setOnItemClickListener(listener);
        listScannedDevices.setAdapter(new DevicesAdapter(this, serial.getScannedDevices()));
        listScannedDevices.setOnItemClickListener(listener);

        return new AlertDialog.Builder(this)
                .setView(dialogView)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        serial.stopDiscovery();
                    }
                })
                .create();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bluetooth 활성화
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        serial = new BluetoothSerial(getApplicationContext(), mBluetoothAdapter);
        serial.setSocketConnectHandler(bluetoothSerialHandler);

        // bluetooth 테스트
        // bluetooth 미지원 기기
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "블루투스를 지원하지 않는 기기입니다.", Toast.LENGTH_LONG).show();
            return;
        }
        // 블루투스 실행 여부 테스트
        if (!mBluetoothAdapter.isEnabled()) {
            // 블루투스 기능이 꺼져있는 상태라면 블루투스를 켜도록 유도
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            // 이후 onActivityResult에서 다음 처리
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        } else {
            // 블루투스 기능이 켜져 있는 상태
            findViewById(R.id.btn_dialog).setOnClickListener(new OnDialogButtonClickListener());
        }

        labelCurrentConnected = (TextView) findViewById(R.id.label_current_connected);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            // 블루투스 기능이 켜진 상태
            findViewById(R.id.btn_dialog).setOnClickListener(new OnDialogButtonClickListener());
        } else {
            // 사용자가 블루투스 기능 켜기를 거부한 상태
            Toast.makeText(this, "블루투스를 활성화시켜야 합니다.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            serial.closeConnection();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class DevicesAdapter extends BaseAdapter {
        Context mContext;
        Set<BluetoothDevice> deviceSet;

        public DevicesAdapter(Context context, Set<BluetoothDevice> deviceSet) {
            this.mContext = context;
            this.deviceSet = deviceSet;
        }

        @Override
        public int getCount() {
            return deviceSet.size();
        }

        @Override
        public Object getItem(int position) {
            return deviceSet.toArray()[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView;
            BluetoothDevice device = (BluetoothDevice) getItem(position);

            if (convertView != null) {
                itemView = convertView;
            } else {
                itemView = LayoutInflater.from(mContext).inflate(R.layout.device_item, parent, false);
            }

            ((TextView) itemView.findViewById(R.id.label_device_name)).setText(device.getName());
            ((TextView) itemView.findViewById(R.id.label_device_address)).setText(device.getAddress());

            return itemView;
        }
    }

    private class OnDeviceClickListener implements AdapterView.OnItemClickListener {
        private Context mContext;

        public OnDeviceClickListener(Context context) {
            this.mContext = context;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BluetoothDevice device = (BluetoothDevice) parent.getAdapter().getItem(position);
            //String text = device.getName() + " " + device.getAddress() + "\n" + device.getUuids()[0].getUuid();

            //Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();

            serial.connectDevice(device);
        }
    }

    private class OnDialogButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getBluetoothDeviceDialog().show();
        }
    }
}
